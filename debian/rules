#!/usr/bin/make -f

export LC_ALL=C.UTF-8

# DH_VERBOSE := 1

%:
	dh $@ --with javahelper --with jh_maven_repo_helper

override_dh_auto_clean:
	mkdir -p build
	dh_auto_clean
	$(RM) pom.xml
	rm -f fastutil-*.jar
	rm -rf build dist docs

override_dh_clean:
	find src/it/unimi/dsi/fastutil -name "*.[ch]" -delete
	# delete auto-generated *.java files.
	# Attention: This
	# 	find src/it/unimi/dsi/fastutil -mindepth 2 -name "*.java" -delete
	# does not work because files in /io need to remain
	# Also package-info.java files need to remain
	find \
		src/it/unimi/dsi/fastutil/booleans \
		src/it/unimi/dsi/fastutil/bytes \
		src/it/unimi/dsi/fastutil/chars \
		src/it/unimi/dsi/fastutil/doubles \
		src/it/unimi/dsi/fastutil/floats \
		src/it/unimi/dsi/fastutil/ints \
		src/it/unimi/dsi/fastutil/longs \
		src/it/unimi/dsi/fastutil/objects \
		src/it/unimi/dsi/fastutil/shorts \
		-name "*.java" -a ! -name "package-info.java" -delete
	rm -f src/it/unimi/dsi/fastutil/io/BinIO.java src/it/unimi/dsi/fastutil/io/TextIO.java
	# Removing test reports
	rm -rf reports
	dh_clean

override_dh_auto_build:
	make sources
	# make sources TEST=1		# will compile behavioral and speed tests into the classes
	# make sources ASSERTS=1	# will compile assertions into the classes
	ant jar

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	ant -Duser.name debian junit
endif

override_mh_installpoms:
	# generating the pom file with the version of the package
	ant pom
	mh_installpoms

execute_before_jh_installjavadoc:
	# Generating the javadoc before installing it
	ant javadoc
